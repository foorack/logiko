"use strict";
window.onload = function() {
    $('#process-form').submit(function(ev) {
        ev.preventDefault(); // to stop the form from submitting
        // clear previous output
        document.getElementById("output-data").innerHTML = "";
        var input = document.getElementById("input-data").value;
        input = document.getElementById("input-data").value = input.trim().toLowerCase();
	document.getElementById("command").innerHTML = input;
        if (input.length == 0) {
            return;
        }
        outputText("Input query:", input);
        var variables = getVariables(input);
        if(variables === null){
	    return;
	}
        outputText("Variables:", variables.join(", "));
        var full = input, i = 0, max = 50;
	for(; i != max && (i == 0 || (full[0] !== "(" && full[full.length - 1] !== ")")); i++) {
		console.log(1, full);
	    full = preParse(full, variables);
	    if (full === null) {
		i === max;
		break;
            }
        }
	if(i === max || full === null){
	    error("Unexpected runtime.", "A part of the code produced unexpected results. Please verify the results manually and debug.");
	    return;
	}
	document.getElementById("command").innerHTML = full;
        outputText("Extended query:", full);
        var tree = parse(full.substring(1, full.length - 1), 0);
        if(tree === null){
	    return;
	}
        console.log(tree);
        outputText("<br>", "");
	outputTable("truth", "Truth table:", ["#"].concat(variables.concat("output")));
        for(var i = 0; i < Math.pow(2, variables.length); i++){
            var data = [];
            data.push(" " + i);
            for(var j in variables){
                data.push(getVar(variables[j], variables, i));
            }
            data.push(calc(tree, variables, i) ? 1 : 0);
            outputTableData("truth", data);
        }
        outputText("<br>", "");
	drawLogicCircuit("circuit", "Logic circuit:", tree, variables);
    });
};

function drawLogicCircuit(id, title, tree, variables) {

        var calcDepth = function(tree, i) {
	    var d = i, e = i;
	    if(typeof tree[0] === "object")
		d = calcDepth(tree[0], i + 1);
	    if(tree[1] !== "not" && typeof tree[2] === "object")
		e = calcDepth(tree[2], i + 1);
	    return Math.max(d, e);
        }
	var depth = calcDepth(tree, 1);
        var countDepth = function(tree, d, i) {
            if(d === i)
	        return 1;
            var c = 0;
	    if(typeof tree[0] === "object")
		c += countDepth(tree[0], d, i + 1);
            else
		if(d === i + 1)
		    c++;
	    if(tree[1] !== "not")
		if(typeof tree[2] === "object")
		    c += countDepth(tree[2], d, i + 1);
		else
		    if(d === i + 1)
			c++;
		
	    return c;
        }
	var maxd = 0;
	for(var i = 1; i <= depth; i++){
	    maxd = Math.max(maxd, countDepth(tree, i, 1));
	}

        var colw = 20, colh = 20;
	var canvas = outputCanvas("circuit", "Logic circuit:", (depth * 4 + (variables.length * 2) + 2 + 4) * colw, Math.max((variables.length * 3 + 1) * colh, (maxd * 2 + 1) * colh));
	var ctx = canvas.getContext("2d");
        var w = canvas.width, h = canvas.height;

        var drawDot = function(x, y, radius, color) {
	    ctx.beginPath();
            ctx.arc(x * colw, y * colh, radius, 0, 2 * Math.PI, false);
            ctx.fillStyle = color;
            ctx.fill();
        }
        var drawLine = function(x1, y1, x2, y2, color) {
	    ctx.lineWidth = 2;
	    ctx.strokeStyle = color;
	    ctx.beginPath();
	    ctx.moveTo(x1 * colw, y1 * colh);
	    ctx.lineTo(x2 * colw, y2 * colh);
	    ctx.stroke();
        }
        var drawFork = function(space, x, y, color1, color2) {
	    drawLine(x - 1.5, y + 0.25, x, y + 0.25, color1); //base
	    drawLine(x - 1.5, y - Math.floor(space / 2) - 0.25, x - 1.5, y + 0.25, color1); // branch
	    drawLine(x - 3, y - Math.floor(space / 2) - 0.25, x - 1.5, y - Math.floor(space / 2) - 0.25, color1);


	    drawLine(x - 1.5, y + 0.75, x, y + 0.75, color2); // base
	    drawLine(x - 1.5, y + Math.floor(space / 2) + 1.25, x - 1.5, y + 0.75, color2); // branch
	    drawLine(x - 3, y + Math.floor(space / 2) + 1.25, x - 1.5, y + Math.floor(space / 2) + 1.25, color2);
        }
        var drawLogicNot = function(x, y, color) {
	    ctx.lineWidth = 1;
	    ctx.beginPath();
	    ctx.moveTo((x + 1) * colw, (y + 0.2) * colh);
	    ctx.lineTo((x + 1.5) * colw, (y + 0.5) * colh);
	    ctx.stroke();
	    ctx.lineWidth = 2;
	    ctx.strokeStyle = color;
	    ctx.beginPath();
	    ctx.strokeRect(x * colw, y * colh, colw, colh);
	    ctx.fillText("1", colw / 5 + x * colw, colh * y + (colh / 1.2));
        }
        var drawLogicAnd = function(x, y, color) {
	    ctx.lineWidth = 2;
	    ctx.strokeStyle = color;
	    ctx.beginPath();
	    ctx.strokeRect(x * colw, y * colh, colw, colh);
	    ctx.fillText("&", colw / 5 + x * colw, colh * y + (colh / 1.2));
        }
        var drawLogicOr = function(x, y, color) {
	    ctx.lineWidth = 2;
	    ctx.strokeStyle = color;
	    ctx.beginPath();
	    ctx.strokeRect(x * colw, y * colh, colw, colh);
	    ctx.fillText("≥", colw / 5 + x * colw, colh * y + (colh / 1.2));
        }

	// draw grid
        for(var i = 1; i < w / colw; i++) {
	    drawLine(i, 0, i, h / colh, "#C0C0C0");
        }
        for(var i = 1; i < h / colh; i++) {
	    drawLine(0, i, w / colw, i, "#C0C0C0");
        }

	// draw variables
	ctx.font = colh + "px Arial";
	ctx.fillStyle = "#000000";
	ctx.strokeStyle = "#000000";
	ctx.lineWidth = 2;
	for(var i = 0; i != variables.length; i++) {
	    ctx.fillText(variables[i], colw / 5, colh * (i * 3 + 1) + (colh / 1.2));
	    //ctx.fillText(variables[i] + "'", colw / 5, colh * (i * 3 + 2) + (colh / 1.2));
            // connector-connectors
	    //drawLine(1, i * 3 + 1.5, i * 2 + 2.5, i * 3 + 1.5);
	    //drawLine(1, i * 3 + 2.5, i * 2 + 3.5, i * 3 + 2.5);
            // connector-dot
            //drawDot(i * 2 + 2.5, i * 3 + 1.5, 3, "#000000");
            //drawDot(i * 2 + 3.5, i * 3 + 2.5, 3, "#000000");
            // connectors
	    drawLine(i * 2 + 2.5, 0, i * 2 + 2.5, h);
	    //drawLine(i * 2 + 3.5, 0, i * 2 + 3.5, h);
        }
	// output
	ctx.fillText("out", w - 2 * colw + colw / 4, colh * (((h / colh) - ((h / colh) % 2 == 0 ? 2 : 1)) / 2) + (colh / 1.25));

	var n = 0, nat = 0; // nat - number at depth
        var space = function(d){
		if(d === 1){
		    return 0;
		}
		return space(d - 1) * 2 + 1;
	}
        var drawDepth = function(tree, d, i) {
	    if(d === i){
		var posx = (depth - i) * 4 + 3 + (variables.length * 2) + 2;
		var posy = (((h / colh) - ((h / colh) % 2 == 0 ? 2 : 1)) / 2) - nat + (n * (space(depth - d + 2) + 1)) + 1 - (space(depth - d + 2) > 1 && d !== 1 ? 3 : 0);
		n++;
		switch(tree[1]){
		case "not":
			drawLogicNot(posx, posy, "#000000");
	                drawLine(variables.indexOf(tree[0]) * 2 + 2.5, posy + 0.5, posx, posy + 0.5);
			drawDot(variables.indexOf(tree[0]) * 2 + 2.5, posy + 0.5, 3, "#000000");
			break;
		case "and":
			drawLogicAnd(posx, posy, "#000000");
			drawFork(space(depth - d + 1), posx, posy, "#000000", "#000000");
			break;
		case "or":
			drawLogicOr(posx, posy, "#000000");
			drawFork(space(depth - d + 1), posx, posy, "#000000", "#000000");
			break;
		}
	 	return;
	    }
	    if(typeof tree[0] === "object"){
		drawDepth(tree[0], d, i + 1);
            }
	    if(tree[1] !== "not" && typeof tree[2] === "object"){
		drawDepth(tree[2], d, i + 1);
            }
        }
	for(var i = depth; i != 0; i--) {
	    n = 0, nat = countDepth(tree, i, 1);
	    drawDepth(tree, i, 1);
	}
}

function calc(tree, vars, st){
    var b1 = false, b2 = false;
    if(typeof tree[0] === "string")
        b1 = getVar(tree[0], vars, st) == 1;
    else
        b1 = calc(tree[0], vars, st);

    if(tree.length === 3 && tree[2] !== null)
        if(typeof tree[2] === "string")
            b2 = getVar(tree[2], vars, st) == 1;
	else
            b2 = calc(tree[2], vars, st);

    switch (tree[1]) {
    case "not":
	return !b1;
    case "and":
	return b1 && b2;
    case "or":
	return b1 || b2;
    case "nand":
	return !(b1 && b2);
    case "nor":
	return !(b1 || b2);
    case "xor":
	return b1 != b2;
    case "xnor":
	return b1 == b2;
    }
}

function getVar(v, vars, st){
    return getBit(st, vars.length - 1 - vars.indexOf(v));
}

function parse(text, j){
    text = text + " ";
    var node = [null, null];
    var tmp = "", not = false, b = 0;
    for (var i = 0; i < text.length; i++){
        var c = text[i];
	console.log(i, b, 888, c, not, tmp);
        if(c === "(") {
            b++;
        }
        if(c === ")") {
            b--;
            if(b === 0) {
                tmp = parse(tmp.substring(1), i);
		continue;
            }
        }
        if(c === " " && b === 0) {
            if((typeof tmp === "string") && isOperator(tmp)){
                node[1] = tmp;
            } else {
		if(node[0] === null){
		    node[0] = tmp;
		} else {
		    node[2] = tmp;
                    break;
		}
	    }
            tmp = "";
            continue;
        }
        tmp += c;
    }
    if(node[1] !== "not" && node[2] === null){
	error("Failed to parse input query.", "Expected variable at position " + (j + text.length - 1) + ".", text + " ", (j + text.length - 1), (j + text.length));
        console.log(378, j, text.length);
        console.log(node);
	return null;
    }
    return node;
}

var stop = 0;
function preParse(text, variables, original = true) {
    if(original)stop = 0;
    text = text + " ";
    var tmp = "", v1 = null, type = null, b = 0, fv = false, not = false;
    for (var i = 0; i < text.length; (i++ && stop++)) {
        if(stop > 5000){
	    error("Failed to parse input query.", "Parsing timed out, too complex or invalid query.");
	    return null;
         }
        var c = text[i];
	console.log(stop, i, b, text, 999999, c, v1, type, not, tmp);
        if (c === '(') {
	    b++;
        } else if ( c === ')') {
            b--;
            if(b === 0){
                fv = true;
                tmp = tmp + ")";
		    var tmp2 = preParse(tmp.substring(1, tmp.length - 1), variables, false);
		    if(tmp2 === null){
			return null;
                    }
		    tmp2 = tmp2.trim();
		    text = text.substring(0, (i) - tmp.length) + tmp2 + text.substring(i + 1);
                    i = i + (tmp2.length - tmp.length);
		    tmp = tmp2;
            }
        }
        if ((fv || c === ' ') && b === 0) {
            if(variables.indexOf(tmp) !== -1 || fv){
	        fv = false;
		if(not){
		    var tmp2 = "(not " + tmp + ")";
                    text = text.substring(0, i - 4 - tmp.length) + tmp2 + text.substring(i);
		    tmp = tmp2;
		    i = i + 2;
		    not = false;
		}
		if(v1 === null){
		    v1 = tmp;
                    tmp = "";
		    continue;
		}
		if(type === null){
		    error("Failed to parse input query.", "Missing operator at position " + (i - tmp.length) + ": '" + tmp + "'", text, (i - tmp.length), i);
		    return null;
		} else {
		    var tmp2 = v1 + " " + type + " " + tmp;
		    text = text.substring(0, (i) - tmp2.length) + "(" + tmp2 + ")" + text.substring(i);
		    i = i + 2;
	    	    v1 = "(" + tmp2 + ")";
		    type = null;
		}
	    } else if(isOperator(tmp)) {
		if(tmp === "not"){
		    not = true;
		} else if(type !== null){
		    error("Failed to parse input query.", "Unexpected operator at position " + (i - tmp.length) + ": '" + tmp + "'", text, (i - tmp.length), i);
		    return null;
                } else {
                    if(v1 === null){
			error("Failed to parse input query.", "Expected variable at position " + (i - tmp.length) + ".", text, (i - tmp.length), i);
		        return null;
		    }
		    type = tmp;
		}
	    }
            tmp = "";
            continue;
        }
        tmp += c;
    }
    return text.trim();
}

function isOperator(text){
        if (typeof text === "object"){
            return isOperator(text[1]);
        }
        if (text.toLowerCase() === "not") return true;
        if (text.toLowerCase() === "and") return true;
        if (text.toLowerCase() === "or") return true;
        if (text.toLowerCase() === "nand") return true;
        if (text.toLowerCase() === "nor") return true;
        if (text.toLowerCase() === "xor") return true;
        if (text.toLowerCase() === "xnor") return true;
	return false;
}

function getVariables(text) {
    var result = [];
    var check = function(t, i) {
        if (t.length === 0) return 1;
        if (isOperator(t)) return 1;
        if (t.match(/[^0-9a-z]/i)) {
            error("Failed to parse input query.", "Invalid variable name at position " + (i - t.length) + ": '" + t + "'", text, (i - t.length), i);
            return -1;
        }
        if (result.indexOf(t) === -1) result.push(t);
	return 0;
    };
    var tmp = "";
    for (var i = 0; i < text.length; i++) {
        var c = text[i];
        if (c === ' ' || c === '(' || c === ')') {
            if(check(tmp, i) < 0){
		return null;
	    }
            tmp = "";
            continue;
        }
        tmp += c;
    }
    if(check(tmp, i) < 0){
	return null;
    }
    text = "";
    return result;
}

function error(title, text, input, start = -1, end = -1) {
    var con = document.createElement("div");
    con.setAttribute("class", "bg-danger mb-3 p-3");
    con.innerHTML = "<b>" + title + "</b> " + text;
    var container = document.getElementById('output-data');
    container.insertBefore(con, container.firstChild);

    if(start !== -1){
	if(end === -1){
	    end = start + 1;
	}
	var output = "<span>" + input.substring(0, start) + "</span>";
	var mid = input.substring(start, end);
	if(mid === " "){
	    output += "<span class=\"bg-danger text-danger\">_</span>";
	} else {
	    output += "<span class=\"bg-danger\">" + input.substring(start, end) + "</span>";
	}
	output += "<span>" + input.substring(end, input.length) + "</span>";
	document.getElementById("command").innerHTML = output
    }
}

function warning(title, text) {
    var con = document.createElement("div");
    con.setAttribute("class", "bg-warning mb-3 p-3");
    con.innerHTML = "<b>" + title + "</b> " + text;
    var container = document.getElementById('output-data');
    container.insertBefore(con, container.firstChild);
}

function outputText(title, text) {
    var con = document.createElement("div");
    con.setAttribute("id", "output" + (document.getElementById("output-data").children.length + 1));
    con.innerHTML = "<b>" + title + "</b> " + text;
    document.getElementById("output-data").append(con);
}

function outputCanvas(id, title, width, height) {
    outputText(title, "");
    var con = document.createElement("div");
    con.setAttribute("id", "output-canvas-container-" + id);
    var canvas = document.createElement("canvas");
    canvas.setAttribute("id", "output-canvas-" + id);
    canvas.setAttribute("height", height);
    canvas.setAttribute("width", width);
    con.append(canvas)
    document.getElementById("output-data").append(con);
    return canvas;
}

function outputTable(id, title, fields) {
    outputText(title, "");
    var table = document.createElement("table");
    table.setAttribute("id", "output-table-" + id);
    table.setAttribute("class", "table bg-primary");
    var thead = document.createElement("thead");
    table.append(thead);
    var tbody = document.createElement("tbody");
    table.append(tbody);
    var tr = document.createElement("tr");
    thead.append(tr);
    for(var i in fields){
        var th = document.createElement("th");
	th.setAttribute("class", "text-center");
        th.innerText = fields[i];
        tr.append(th);
    }
    document.getElementById("output-data").append(table);
}

function outputTableData(id, fields) {
    var tr = document.createElement("tr");
    for(var i in fields){
        var th = document.createElement("th");
	th.setAttribute("class", "text-center");
        th.innerText = fields[i];
	if(fields[i] == "1")
            th.setAttribute("class", "bg-success text-center");
	if(fields[i] == "0")
            th.setAttribute("class", "bg-danger text-center");
        tr.append(th);
    }
    document.getElementById("output-table-" + id).children[1].append(tr);
}

function getBit(num, bit){
    return ((num>>bit) % 2 != 0) ? 1 : 0;
}
